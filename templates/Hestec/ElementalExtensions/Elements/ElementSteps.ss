<div class="container $ExtraClass">
	<% if $ShowTitle %><h2 class="<% if $TitleCenter %>text-center<% end_if %><% if $HSize && $HSize != normal %> $HSize<% end_if %>">$Title</h2><% end_if %>
	<% if $Content %>$Content<% end_if %>
	<% if $Steps %>
    <div class="row step-pointers">
    <% loop $Steps %>
        <div class="col-12 col-lg step-col">
            <div class="step-pointer h-100">
                <div class="text-center">
                    <h5>$Title</h5>
                    $Content
                    <% if $Image %>$Image<% end_if %>
                </div>
                <div class="step-badge"></div><div class="mask-t"><span class="sale-text kalam">$Pos</span></div>
            </div>
        </div>
    <% end_loop %>
    </div>
	<% end_if %>
</div>
