<?php

namespace Hestec\ElementalExtensions\Dataobjects;

use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;
use Hestec\ElementalExtensions\Elements\ElementSteps;
use SilverStripe\Forms\FieldList;
use SilverStripe\Security\Permission;
use SilverStripe\Assets\Image;
use SilverStripe\AssetAdmin\Forms\UploadField;

class Step extends DataObject {

    private static $table_name = 'HestecElementStep';

    private static $singular_name = 'Step';
    private static $plural_name = 'Steps';

    private static $db = [
        'Title' => 'Varchar(255)',
        'SubTitle' => 'Varchar(255)',
        'Content' => 'HTMLText',
        'Sort' => 'Int'
    ];

    private static $has_one = [
        'ElementSteps' => ElementSteps::class,
        'Image' => Image::class,
    ];

    private static $owns = [
        'Image'
    ];

    private static $summary_fields = [
        'Title' => 'Title'
    ];

    private static $default_sort = 'Sort';

    public function getCMSFields()
    {
        $TitleField = TextField::create('Title', "Title");
        $SubTitleField = TextField::create('SubTitle', "SubTitle");
        $ContentField = HTMLEditorField::create('Content', "Content");
        $ImageField = UploadField::create('Image', "Image");
        $ImageField->setDescription("Max width 150 px.");

        return new FieldList(
            $TitleField,
            $SubTitleField,
            $ContentField,
            $ImageField
        );

    }

    public function canView($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canEdit($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canDelete($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canCreate($member = null, $context = [])
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

}
