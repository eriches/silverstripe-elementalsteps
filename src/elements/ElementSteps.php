<?php

namespace Hestec\ElementalExtensions\Elements;

use DNADesign\Elemental\Models\BaseElement;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use Hestec\ElementalExtensions\Dataobjects\Step;

class ElementSteps extends BaseElement
{

    private static $table_name = 'HestecElementSteps';

    private static $singular_name = 'Step';

    private static $plural_name = 'Steps';

    private static $description = 'Element with steps';

    private static $icon = 'font-icon-checklist';

    private static $db = [
        'Content' => 'HTMLText',
        'TitleCenter' => 'Boolean'
    ];

    private static $has_many = array(
        'Steps' => Step::class
    );

    private static $inline_editable = false;

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $ContentField = HTMLEditorField::create('Content', "Content");
        $ContentField->setRows(5);

        $TitleCenterField = CheckboxField::create('TitleCenter' , "TitleCenter");

        $fields->addFieldToTab('Root.Main', $TitleCenterField);
        $fields->addFieldToTab('Root.Main', $ContentField);

        if ($this->ID) {

            $StepsGridField = GridField::create(
                'Steps',
                'Steps',
                $this->Steps(),
                GridFieldConfig_RecordEditor::create()
                    ->addComponent(new GridFieldOrderableRows())
            );

            //$field = TextField::create('test', 'test');
            $fields->addFieldToTab('Root.Main', $StepsGridField);
        }
        return $fields;
    }

    public function getType()
    {
        return 'Steps';
    }
}